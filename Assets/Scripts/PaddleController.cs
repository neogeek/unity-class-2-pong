﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    public KeyCode up;
    public KeyCode down;

    public Transform ball;

    public bool isAI = false;

    private readonly float speed = 30;

    void Update() {

        if (isAI) {

            gameObject.transform.position = new Vector3(
                gameObject.transform.position.x,
                Mathf.Lerp(gameObject.transform.position.y, ball.transform.position.y, speed * Time.deltaTime),
                gameObject.transform.position.z
            );

        } else {

            if (Input.GetKey(up)) {

                gameObject.transform.Translate(0, speed * Time.deltaTime, 0);

            } else if (Input.GetKey(down)) {

                gameObject.transform.Translate(0, -speed * Time.deltaTime, 0);

            }

        }

        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x,
            Mathf.Clamp(gameObject.transform.position.y, -12f, 12f),
            gameObject.transform.position.z
        );

    }

    void OnTriggerEnter(Collider other) {

        if (!isAI && other.gameObject.name.Equals("Ball") &&
            (Mathf.Sign(other.gameObject.GetComponent<Rigidbody>().velocity.x) ==
                Mathf.Sign(other.gameObject.transform.position.x + gameObject.transform.position.x))) {

            Time.timeScale = 0.25f;

        } else {

            Time.timeScale = 1;

        }

    }

    void OnTriggerExit(Collider other) {

        Time.timeScale = 1;

    }

}
