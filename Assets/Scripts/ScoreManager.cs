﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

    public int scoreP1 = 0;
    public int scoreP2 = 0;

    public int winner = 0;

    private void OnGUI() {

        if (winner != 0) {

            if (GUI.Button(new Rect(100, 100, 200, 100), "New Game")) {

                SceneManager.LoadScene("Main");

            }

        }

        if (winner == 1) {

            GUI.Box(new Rect(10, 10, 120, 30), "Player 1 Wins!!!!!!!");

        } else if (winner == 2) {

            GUI.Box(new Rect(10, 10, 120, 30), "Player 2 Wins!!!!!!!");

        } else {

            GUI.Box(new Rect(10, 10, 90, 30), "Player 1: " + scoreP1);
            GUI.Box(new Rect(100, 10, 90, 30), "Player 2: " + scoreP2);

        }

    }

}
