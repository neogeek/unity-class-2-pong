﻿using System.Collections;
using UnityEngine;

public class BallController : MonoBehaviour {

    private Rigidbody rb;
    private new Renderer renderer;

    private AudioSource bounceSFX;

    private float launchDirection = 1;

    private readonly WaitForSeconds launchBallDelay = new WaitForSeconds(2);

    void Awake() {

        rb = gameObject.GetComponent<Rigidbody>();
        renderer = gameObject.GetComponent<Renderer>();

        bounceSFX = gameObject.GetComponent<AudioSource>();

    }

    void Start() {

        StartCoroutine("BallStart");

    }

    IEnumerator BallStart() {

        gameObject.SetActive(true);

        rb.velocity = Vector3.zero;

        gameObject.transform.position = Vector3.zero;

        yield return launchBallDelay;

        BallStart();

        rb.AddForce(Random.Range(8, 12) * launchDirection, Random.Range(-4, 4), 0);

    }

    void OnTriggerEnter(Collider other) {

        if (other.isTrigger && other.name.Contains("Playfield") && gameObject.activeSelf) {

            launchDirection = Mathf.Sign(gameObject.transform.position.x);

            StartCoroutine("BallStart");

        }

    }

    void OnCollisionEnter(Collision other) {

        if (other.gameObject.tag.Equals("Paddle")) {

            bounceSFX.panStereo = Mathf.Sign(other.gameObject.transform.position.x);

            rb.velocity = rb.velocity * 1.05f;

        }

    }

}
