﻿using UnityEngine;

public class PlayfieldController : MonoBehaviour {

    public ScoreManager scoreManager;

    private AudioSource victorySFX;

    void Awake() {

        victorySFX = gameObject.GetComponent<AudioSource>();

    }

    void OnTriggerEnter(Collider other) {

        if (other.name.Contains("Ball")) {

            float ballPos = other.transform.position.x;

            if (ballPos < 0) {

                scoreManager.scoreP1 += 1;

            } else {

                scoreManager.scoreP2 += 1;

            }

            victorySFX.panStereo = Mathf.Sign(other.gameObject.transform.position.x);

            victorySFX.Play();

            if (scoreManager.scoreP1 >= 5) {

                scoreManager.winner = 1;

                other.gameObject.SetActive(false);

            } else if (scoreManager.scoreP2 >= 5) {

                scoreManager.winner = 2;

                other.gameObject.SetActive(false);

            }

        }

    }

}
